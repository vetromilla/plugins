jQuery(document).ready(function($) {
	var custom_file_frame;
	var $container = $('#meta-box-mip .images ul');
	var $message   = $('.message');
	var $form      = $('#post');
	$("#sortable").sortable();
	$("#sortable").disableSelection();

	$(document).on('click', 'input[name=addimage]', function(e) {
		e.preventDefault();
		if (typeof(custom_file_frame) !== "undefined") {
			custom_file_frame.close();
		}

		custom_file_frame = wp.media.frames.customHeader = wp.media({
			title: "Adicionar imagens",
			library: {
				type: 'image'
			},
			button: {
				text: "Ok"
			},
			multiple: true
		});

		custom_file_frame.on('select', function() {
			var selection = custom_file_frame.state().get('selection');
			$message.text('');
			selection.map(function(attachment) {
				attachment = attachment.toJSON();
				$li = $(
					'<li class="ui-state-default">' + 
					'	<input type="hidden" name="image[]" value="' + attachment.id + '" />' + 
					'	<img src="' + attachment.url + '" title="' + attachment.alt + '" alt="' + attachment.alt + '"/>' + 
					'</li>'
				);
				$.post(ajaxurl, { action : 'mip_insert_image', post : post_id, image : attachment.id });
				$container.append($li);
			});
		});

		custom_file_frame.open();
	});

	$('.remove').on('click', function(e) {
		var id  = $(this).attr('rel');
		var $li = $(this).parent();

		$.post(ajaxurl, { action : 'mip_delete_image', post : post_id, order : $li.index() }, function(o) {
			if (o && o == 1) {
				$li.fadeOut('fast', function() {
					$(this).remove();
				});
			} else {
				alert("Erro ao tentar remover a imagem");
			}
		});

		e.preventDefault();
	});

	$("#sortable").on("sortupdate", function() {
		var $images = $form.find('input[name=image\\[\\]]');
		var images = [];
		for (i = 0; i < $images.size(); i++) {
			images.push( $images.eq(i).val() );
		}
		$.post(ajaxurl, { action : 'mip_sort_images', post : post_id, images : images });
	});
});