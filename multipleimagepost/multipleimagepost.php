<?php
/*
Plugin Name: Multiple Images inside Posts
Plugin URI: http://sem.pagina.com
Description: Vincula várias imagens em posts.
Version: 1.0
Author: Cucets
Author URI: http://sem.pagina.com
*/

// Cadastra todos os posts types para permitir receber imagens
function mip_activate() {
	$types = array_merge(

		// Post type original
		array('post'), 

		// Custom posts types
		array_keys( get_post_types( array('_builtin' => false) ) )
	);
	update_option('mip_post_types', $types);
}
register_activation_hook(__FILE__, 'mip_activate');

// Limpa lixo de registros e garante desinstalação segura
function mip_uninstall() {
	delete_option('mip_post_types');
}
register_uninstall_hook(__FILE__, 'mip_uninstall');

// Carrega dependências (JS, CSS, etc)
function mip_enqueue_dependencies() {
	global $wp_version;
	if ( is_admin() ) {
		if ( function_exists('wp_enqueue_media') && version_compare($wp_version, '3.5', '>=') ) {
			wp_enqueue_media();
		} else {
			wp_enqueue_script('media-upload');
			wp_enqueue_script('thickbox');
			wp_enqueue_style('thickbox');
		}

		wp_enqueue_style('media');
		wp_enqueue_script('jquery-ui-sortable', false, array('jquery'));
		wp_enqueue_script('mip-js', plugin_dir_url(__FILE__) . 'multipleimagepost.js', array(), false, true);
		wp_enqueue_style('mip-css', plugin_dir_url(__FILE__) . 'multipleimagepost.css');		
	}
}
add_action('admin_enqueue_scripts', 'mip_enqueue_dependencies');

// Registra metabox para adição de imagens em posts que permitam recebe-las
function mip_register_metabox($post_type) {
	$types = get_option('mip_post_types');

	foreach($types as $type) {
		add_meta_box(
			'meta-box-mip',
			'Imagens associadas',
			'mip_manage_metabox',
			$type,
			'normal'
		);
	}
}
add_action('add_meta_boxes', 'mip_register_metabox', 10, 1);

// Gerencia metabox de imagens
function mip_manage_metabox($post) {

	// Nonce
	wp_nonce_field('meta-box-mip', 'meta-box-mip-nonce');

	// Pega as imagens existentes
	$images = mip_get_images($post->ID);
	if ( empty($images) ) : ?><div class="message"><h2>Nenhuma imagem encontrada</h2></div><?php endif; ?>
	<div class="images">
		<ul id="sortable" class="ui-state-default">
			<?php if (isset($images) && is_array($images) && count($images) > 0) : $i = 0; foreach($images as $image) : ?>
			<?php $image = mip_get_image_data($image); ?>
			<li>
				<div class="remove"></div>
				<input type="hidden" name="image[]" value="<?php echo $image->ID; ?>" />
				<img src="<?php echo $image->guid; ?>" title="<?php echo $image->post_title; ?>" alt="<?php echo $image->post_title; ?>" />
			</li>
			<?php $i++; endforeach; endif; ?>
		</ul>
		<div class="clear"></div>
	</div>
	<input type="button" name="addimage" class="button button-primary" value="Adicionar imagens" />
	<script type="text/javascript">var post_id = '<?php echo $post->ID; ?>'; </script><?php
}

// Adiciona imagens via post save/edit
function mip_save_post($post_id) {
	if ( !isset($_POST['meta-box-mip-nonce']) ) { 
		return $post_id;
	}

	$nonce = $_POST['meta-box-mip-nonce'];
	if ( !wp_verify_nonce($nonce, 'meta-box-mip') ) {
		return $post_id;
	}

	if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) { 
		return $post_id;
	}

	if ( !current_user_can('edit_post', $post_id) ) { 
		return $post_id;
	}

	if ( !empty($_POST['image']) && is_array($_POST['image']) ) {
		update_post_meta($post_id, 'mip_images', $_POST['image']);
	}
}
add_action('save_post', 'mip_save_post');

// Adiciona imagens via Ajax
function mip_ajax_insert_image() {
	$post_id = $_POST['post'];
	$image   = $_POST['image'];

	$images = mip_get_images($post_id);
	if ( !is_array($images) ) { $images = array(); }
	array_push($images, $image);
	update_post_meta($post_id, 'mip_images', $images);
}
add_action('wp_ajax_mip_insert_image', 'mip_ajax_insert_image');

// Deleta imagens via Ajax
function mip_ajax_delete_image() {
	$post_id = $_POST['post'];
	$order   = $_POST['order'];

	$images  = mip_get_images($post_id);
	unset($images[ $order ]);

	update_post_meta($post_id, 'mip_images', $images);
	die('1');
}
add_action('wp_ajax_mip_delete_image', 'mip_ajax_delete_image');

// Ordena imagens via Ajax
function mip_ajax_sort_images() {
	$post_id = $_POST['post']; 
	$images  = $_POST['images'];
	
	update_post_meta($post_id, 'mip_images', $images);
}
add_action('wp_ajax_mip_sort_images', 'mip_ajax_sort_images');

// Remove imagem associada quando a mesma for excluida permanentemente no Wordpress
function mip_wp_remove_attachment($image_id) {
	global $wpdb;

	$results = $wpdb->get_results("
		SELECT *
		FROM $wpdb->postmeta
		WHERE meta_key = 'mip_images'
	");

	foreach($results as $result) {
		$post_id = $result->post_id;
		$images  = mip_get_images($post_id);
		$found   = array_keys($images, $image_id);
		foreach($found as $f) { unset($images[ $f ]); }
		update_post_meta($post_id, 'mip_images', $images);
	}
}
add_action('delete_attachment', 'mip_wp_remove_attachment');

/*
 ****************************************************************************
 * API
 ****************************************************************************
*/

/**
 * Retorna todas as imagens associadas a um post
 *
 * @param mixed $post_id O Código do post desejado (Ou nenhum valor caso queira as imagens do post atual)
 * @return mixed Retorna um array com as informações
 */
function mip_get_images($post_id = false) {
	global $post;
	if ( !$post_id && is_object($post) ) { $post_id = $post->ID; }

	return get_post_meta($post_id, 'mip_images', true);
}

/**
 * Retorna todas as informações de uma imagem
 *
 * @param int $image_id O ID da imagem
 * @return WP_Post Retorna um objeto Post 
 */
function mip_get_image_data($image_id) {
	return get_post($image_id);
}
?>