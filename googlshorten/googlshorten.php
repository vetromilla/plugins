<?php
/*
Plugin Name: Google Shortener Permalinks
Plugin URI: http://sem.pagina.com
Description: Encurta permalinks utilizando a API do Google Shortener
Version: 1.0
Author: Cucets
Author URI: http://sem.pagina.com
*/

/**
 * Gera permalink encurtado via Google Shortener API
 *
 * @param int $post_id O Código do post
 * @return boolean Retorna se a operação foi sucedida
 * @see https://developers.google.com/url-shortener/v1/getting_started?hl=pt
 */
function gs_generate_permalink($post_id = false) {
	global $post;

	// Pega o post atual (se for possível)
	if ( !$post_id && is_object($post) ) { $post_id = $post->ID; }

	// Pega o permalink original do post
	$permalink = get_permalink($post_id);

	// Se não existir, o post é inválido ou inexistente
	if (!$permalink) { return false; }

	// Realiza conexão com a API do Google Shortener
	$body = json_encode( array('longUrl' => $permalink) );
	$data = wp_remote_post('https://www.googleapis.com/urlshortener/v1/url', array(
		'headers'   => array(
			'Content-Type'   => 'application/json',
			'Accept'         => 'application/json',
			'Content-Length' => strlen($body)
		),
		'body'      => $body,
		'sslverify' => false
	));

	// Salva o valor como Post Meta
	if ( is_array($data) ) {
		if ( isset($data['body']) ) {
			$body = json_decode($data['body']);
			$url = $body->id;
			update_post_meta($post_id, 'gl_shorten', $url);
			return true;
		}
	}

	return false;
}

// Gera link permanente encurtado 
function gs_save_post($post_id) {
	gs_generate_permalink($post_id);

	// Retorna o próprio código do post
	return $post_id;
}
add_action('save_post', 'gs_save_post');

/**
 * Retorna o permalink curto
 *
 * @param int $post_id O código do post (Se não for passado, será pego o post atual)
 * @return string O link permanente encurtado ou uma string vazia caso nao exista
 */
function gs_get_permalink($post_id = false) {
	global $post;
	
	if ( !$post_id && is_object($post) ) { $post_id = $post->ID; }
	return get_post_meta($post_id, 'gl_shorten', true);
}

/**
 * Echo para exibição de permalink curto
 *
 * @uses gs_get_permalink
 */
function gs_the_permalink($post_id = false) {
	echo gs_get_permalink($post_id);
}

/**
 * Gera link permanente para vários posts
 * @param boolean $all Se for definido, busca por todos os posts, caso contrário, gera apenas para posts que já possuam
 */
function gs_generate_permalinks($all = false) {
	global $wpdb;

	// Array de posts
	$posts = array();

	// Todos os posts
	if ($all) {
		$results = get_posts('posts_per_page=-1');
		foreach($results as $result) { $posts[] = $result->ID; }

	// Apenas posts que já possuam link permanente encurtado
	} else {
		$sql = "SELECT * FROM {$wpdb->postmeta} WHERE meta_key = 'gl_shorten'";
		$results = $wpdb->get_results($sql);
		foreach($results as $result) { $posts[] = $result->post_id; }
	}

	if ( count($posts) > 0) {
		foreach($posts as $post) {
			gs_generate_permalink($post);
		}
	}
}

/**
 * Gera novamente link permanente encurtado quando a sua estrutura for modificada
 */
add_action('update_option_permalink_structure', 'gs_generate_permalinks');