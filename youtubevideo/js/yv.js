(function() {
	tinymce.create('tinymce.plugins.Youtubevideo', {

		init : function(ed, url) {
			ed.addButton('youtubevideo', {
				title : 'Adiciona vídeo de Youtube por sua URL',
				cmd : 'youtubevideo',
				image : url.replace('/js', '') + 'icon.jpg'
			});

			ed.addCommand('youtubevideo', function() {
				var url = prompt("Digite a URL completa do vídeo de Youtube\nEx: http://www.youtube.com/watch?v=123abc"), 
					shortcode;

				if (url != '') {
					if (url.indexOf('watch?v=') != -1) {
						var vid = url.match(/\?v\=(.*?)$/ig);
						if (vid[0]) {
							vid = vid[0].replace('?v=', '');
							url = '//www.youtube.com/embed/' + vid + '?rel=0';
						}
					}

					if (url.indexOf('/embed/') == -1) {
						alert("A URL deve ser válida");
					} else {
						shortcode = '[youtube-video url="' + url + '" /]';
						ed.execCommand('mceInsertContent', 0, shortcode);
					}
				} else {
					alert("A URL deve ser preenchida");
				}
			});
		},

		createControl : function(n, cm) {
			return null;
		},

		getInfo : function() {
			return {
				longname : 'Youtube Video',
				author : 'Cucets',
				authorurl : '',
				infourl : '',
				version : "0.1"
			};
		}
	});

	tinymce.PluginManager.add('youtubevideo', tinymce.plugins.Youtubevideo);
})();