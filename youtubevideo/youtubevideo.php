<?php
/*
Plugin Name: Youube Video Editor Button
Plugin URI: http://sem.pagina.com
Description: Adiciona um pequeno botão no editor de conteúdo TinyMCE para inclusão de vídeos Youtube via shortcode.
Version: 1.0
Author: Cucets
Author URI: http://sem.pagina.com
*/

// Constantes
if ( !defined('YV_PLUGIN_PATH_URL_JS') ) { define('YV_PLUGIN_PATH_URL_JS', plugin_dir_url( __FILE__ ) . '/js'); }

// Setup
function yv_setup_buttons() {
	add_filter("mce_external_plugins", "yv_add_buttons");
	add_filter("mce_buttons", "yv_register_buttons");
}
add_action("init", "yv_setup_buttons");

// Registra shortcode de adição de vídeo youtube
function yv_add_player( $atts ) {
	extract( shortcode_atts(array('url' => ''), $atts) );

	if ( !empty($url) ) {
		return '<iframe width="420" height="315" src="' . $url . '" frameborder="0" allowfullscreen></iframe>';
	}

	return '';
}
add_shortcode('youtube-video', 'yv_add_player');

// Configura a localização do plugin TinyMCE (JS) responsável pelas funcionalidades de adicionar vídeos
function yv_add_buttons($plugin_array) {
	$plugin_array['youtubevideo'] = YV_PLUGIN_PATH_URL_JS . '/yv.js';
	return $plugin_array;
}

// Registra o botão de adicionar vídeo no editor TinyMCE
function yv_register_buttons($buttons) {
    array_push($buttons, 'youtubevideo');
    return $buttons;
}
?>